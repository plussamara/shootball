﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shayba : MonoBehaviour
{
    public float factor = 100.0f;
    public AudioClip shootBall;
    public AudioClip collideBall;

    private float startTime;
    private Vector3 startPos;
    private Rigidbody2D rig;
    private GameManager gm;
    private AudioSource audioS;
    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        gm = GameManager.instance;
        audioS = GetComponent<AudioSource>();
    }
    
    // Update is called once per frame
    void Update()
    {
        if (!gm.IsStartGame)
            return;
#if UNITY_EDITOR

        if (Input.GetMouseButtonDown(0))
        {
            PointDown();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            PointUp();
        }
        
#else

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                PointDown();
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                PointUp();
            }
        }
#endif
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!gm.IsStartGame)
            return;
        audioS.Stop();
        audioS.clip = collideBall;
        audioS.Play();
    }

    void PointDown()
    {
        startTime = Time.time;
        startPos = Input.mousePosition;
        startPos.z = transform.position.z - Camera.main.transform.position.z;
        startPos = Camera.main.ScreenToWorldPoint(startPos);
    }

    void PointUp()
    {
        var endPos = Input.mousePosition;
        endPos.z = transform.position.z - Camera.main.transform.position.z;
        endPos = Camera.main.ScreenToWorldPoint(endPos);

        var force = endPos - startPos;
        force.z = force.magnitude;
        force /= (Time.time - startTime);

        rig.AddForce(force * factor);
        audioS.Stop();
        audioS.clip = shootBall;
        audioS.Play();
    }
}
