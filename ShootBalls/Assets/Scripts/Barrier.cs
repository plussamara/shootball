﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrier : Entity
{

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (gm != null)
        {
            if (collision.transform.CompareTag("Player"))
            {
                gm.OnTakeBarrier();
            }
        }
    }
}
