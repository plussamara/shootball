﻿using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null; // Экземпляр объекта

    public List<LevelScript> levels = new List<LevelScript>();
    public Transform respawnPos;
    public GameObject prefBarrier;
    public GameObject prefGate;

    public Shayba shayba;
    private int currentLevel = 0;
    private bool startGame = false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
    private void Start()
    {
        GameAnalytics.Initialize();
    }
    

    IEnumerator CreateLevel()
    {
        StopShayba();
        shayba.gameObject.SetActive(false);
        yield return new WaitForSeconds(1);
        UIManager.instance.SetLevelTxt(levels[currentLevel].number);
        shayba.gameObject.SetActive(true);
        ClearLevel();
        //Расстанавливаем барьеры
        int bcount = levels[currentLevel].barriers.Count; 
        for (int i = 0; i<bcount; i++)
        {
            var barrier = Instantiate(prefBarrier, respawnPos);
            barrier.transform.position = levels[currentLevel].barriers[i];
        }
        //задаем стартовую позицию Игрока
        shayba.transform.position = levels[currentLevel].startPosShayba;
        //создаем ворота
        var gate = Instantiate(prefGate, respawnPos);
        gate.transform.position = levels[currentLevel].posGate;
        
        startGame = true;
    }
    private void ClearLevel()
    {
        int count = respawnPos.childCount;
        for (int i = 0; i < count; i++)
        {
            Destroy(respawnPos.GetChild(i).gameObject);
        }
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            UIManager.instance.ShowMainMenu();
            startGame = false;
        }
    }
    private void StopShayba()
    {
        var rb = shayba.GetComponent<Rigidbody2D>();
        rb.velocity = Vector2.zero;
        rb.angularVelocity = 0;
    }

    public void StartLevel(int level)
    {
        currentLevel = level;
        
        StartCoroutine(CreateLevel());        
    }
    //Столкновение с барьером
    public void OnTakeBarrier()
    {
        StopShayba();
        //задаем стартовую позицию Игрока
        shayba.transform.position = levels[currentLevel].startPosShayba;
    }
    //попадание в ворота
    public void OnTakeGate()
    {
        StopShayba();
        
        currentLevel++;        
        GameAnalytics.NewDesignEvent("level_complete", currentLevel);
        if (currentLevel < levels.Count)
        {
            StartLevel(currentLevel);
        }
        else
        {
            startGame = false;
            UIManager.instance.OnShowEventPanel("You win!!!");
        }
    }

    public bool IsStartGame
    {
        get
        {
            return startGame;
        }
    }
}
