﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : Entity
{
    


    private void OnTriggerEnter2D(Collider2D collision)
    {       
    
        if (gm != null)
        {
            if (collision.transform.CompareTag("Player"))
            {
                gm.OnTakeGate();
                audioS.Play();
            }
        }
    
    }
}
