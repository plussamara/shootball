﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIManager : MonoBehaviour
{
    public static UIManager instance = null; // Экземпляр объекта

    public GameObject mainMenu;//главное меню    
    public GameObject eventPanel;//панель для сообщения    
    public GameObject levelPanel;
    public Text lvlCurrText;  
    

    private GameManager gm;
    
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        gm = GameManager.instance;
        
        ShowMainMenu();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetLevelTxt(int number)
    {
        lvlCurrText.text = number.ToString();        
    }
    
    public void ShowMainMenu()
    {
        mainMenu.SetActive(true);        
        eventPanel.SetActive(false);
        levelPanel.SetActive(false);
        
    }
    

    public void OnStartBtn()
    {        
        gm.StartLevel(0);        
        mainMenu.SetActive(false);
        levelPanel.SetActive(true);
    }
    public void OnShowEventPanel(string message)
    {
        Transform tr = eventPanel.transform.Find("MessageText");
        if (tr!=null)
        {
            tr.GetComponent<Text>().text = message;
        }
        eventPanel.SetActive(true);
    }

    public void OnExitBtn()
    {
        Application.Quit();
    }
}
